import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SallesService {

  constructor(private http: HttpClient) { }

  getAllSalles(formation) {
    return this.http.get(formation._links.salles.href);
  }
}
