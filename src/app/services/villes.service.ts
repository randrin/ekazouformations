import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VillesService {

  public villestUrl = window['baseUrl'] + '/villes';

  constructor(private http: HttpClient) { }

  getAllVilles() {
    return this.http.get(this.villestUrl);
  }
}
