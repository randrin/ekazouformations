import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FormationsService {

  public urlHost = window['baseUrl'];
  public urlProjection = "?projection=coursProjection";

  constructor(private http: HttpClient) { }

  getAllFormations(ville) {
    return this.http.get(ville);
  }

  getAllProjections(salle) {
    const url = salle._links.projections.href.replace("{?projection}", "");
    return this.http.get(url + this.urlProjection);
  }

  getImageCours(id) {
    return this.http.get(this.urlHost + '/Formations/getImage/' + id);
  }
}
