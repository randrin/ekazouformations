import { Component, OnInit } from '@angular/core';
import { FormationsService } from 'src/app/services/formations.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { SallesService } from 'src/app/services/salles.service';

@Component({
  selector: 'app-formations',
  templateUrl: './formations.component.html',
  styleUrls: ['./formations.component.scss']
})
export class FormationsComponent implements OnInit {

  public hostToBackend;
  public formations;
  public salles;
  public villeSelected;
  public nameVilleSelected;
  public currentFormation;
  public projections;

  constructor(private formationsService: FormationsService, private router: Router, private activatedRoute: ActivatedRoute,
              private sallesService: SallesService) { }

  ngOnInit() {
    this.hostToBackend = this.formationsService.urlHost;
    this.nameVilleSelected = this.activatedRoute.snapshot.params.nameVille;
    this.villeSelected = atob(this.activatedRoute.snapshot.params.ville);
    console.log('this.villeSelected', this.villeSelected);
    this.getAllFormations(this.villeSelected);
  }

  getAllFormations(villeSelected) {
    this.formationsService.getAllFormations(villeSelected).subscribe(response => {
      this.formations = response;
    }, error => {
      console.log('Error getAllFormations(): ', error);
    });
  }

  onGetSalles(formation) {
    this.currentFormation = formation;
    console.log('Formation selected: ', formation.name);
    this.sallesService.getAllSalles(formation).subscribe(response => {
      this.salles = response;
      this.salles._embedded.salles.forEach(salle => {
        this.formationsService.getAllProjections(salle).subscribe(response => {
          salle.projections = response;
        }, error => {
          console.log('Error getAllProjections(): ', error);
        });
      });
    }, error => {
      console.log('Error getAllSalles(): ', error);
    });
  }

  onGetImageCours(idCours) {
    return this.formationsService.getImageCours(idCours);
  }
}
