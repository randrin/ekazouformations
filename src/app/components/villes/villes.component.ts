import { Component, OnInit } from '@angular/core';
import { VillesService } from 'src/app/services/villes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-villes',
  templateUrl: './villes.component.html',
  styleUrls: ['./villes.component.scss']
})
export class VillesComponent implements OnInit {

  public villes;

  constructor(private villesService: VillesService, private router: Router) { }

  ngOnInit() {
    this.onGetAllVilles();
  }

  onGetAllVilles() {
    this.villesService.getAllVilles().subscribe(response => {
      this.villes = response;
    }, error => {
      console.log('Error getAllVilles(): ', error);
    });
  }

  onGetFormations(ville) {
    const villeName = ville.name.replace(/\s/g, "");
    this.router.navigateByUrl('formations/' + villeName + '/' + btoa(ville._links.formations.href));
  }
}
